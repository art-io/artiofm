# New issue

Before you submit your issue, please review the following elements. Check the boxes below to confirm that you have read and understood each item.

- [ ] I have checked that the issue does not already exist or has not already been resolved in another [issue](https://gitlab.com/artios-association/artios/-/issues) or [merge request](https://gitlab.com/artios-association/artios/-/merge_requests).

## Summary

Describe the issue here. The more details you provide, the easier it will be for the team to understand the issue and the faster it will be resolved.

## User story

Describe the user story here.

> **As a** persona,  
> **I want to** task,  
> **So that** goal.

<details>
<summary>Display an example</summary>

> **As a** reader of this issue,  
> **I want to** know how to fill a user story,  
> **So that** I can fill my own user story.
</details>

## Weight

Define the weight of the issue here from 0 to 5 (including bounds) for the following criteria. Round your weight up to the next Fibonacci number (0, 1, 2, 3, 5, 8, 13 or 21).

- Effort / Volume: How much work does this task requires?
- Risk: How much of the integrity of the application is at risk?
- Complexity: How complex are the designs and algorithms we are be dealing with?
- Uncertainty: How much technical knowledge are we missing to complete the task?

<details>
<summary>Display the weights of the team</summary>

| GitLab username      	| Weight (round up to the next Fibonacci number)  |
|-----------------------|------------------------------------------------:|
|                       |                                                 |
</details>

## Useful resources

List the useful resources here if needed (documentation links, posts, etc).

## Related issues

You can link any related issues to this issue after you submit the issue or by using the `/link` [GitLab quick action](https://docs.gitlab.com/ee/user/project/quick_actions.html).

<!-- Assign the issue to myself -->
/assign me
