# New incident

Before you submit your incident, please review the following elements. Check the boxes below to confirm that you have read and understood each item.

- [ ] I have checked that the incident does not already exist or has not already been resolved in another [issue](https://gitlab.com/artios-association/artios/-/issues) or [merge request](https://gitlab.com/artios-association/artios/-/merge_requests).
- [ ] I have tested my issue on the [latest version of Artios](https://gitlab.com/artios-association/artios/-/tree/main).

## Summary

Describe the incident here. The more details you provide, the easier it will be for the team to understand the issue and the faster it will be resolved.

## Current behavior

Describe the current behavior here. The more details you provide, the easier it will be for the team to understand the issue and the faster it will be resolved.

## Expected behavior

Describe the expected behavior here. The more details you provide, the easier it will be for the team to understand the issue and the faster it will be resolved.

## Steps to reproduce

Describe the steps to reproduce the issue here. The more details you provide, the easier it will be for the team to understand the issue and the faster it will be resolved.

## Attempts to solve the incident

Describe the attempts you tried to solve the incident if relevant. The more details you provide, the easier it will be for the team to understand the issue and the faster it will be resolved.

## More information

Provide more information here if needed.
