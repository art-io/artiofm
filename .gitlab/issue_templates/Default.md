💻 Is this an issue for general work? A new feature, documentation or a refactor? Select the issue type **Issue** and the template **Issue**.

🐛 Is this an issue for investigating a problem? A bug, IT service disruptions or outages? Select the issue type **Incident** and the template **Incident**.
