<!-- Assign the merge request to myself -->
/assign me

<!-- Remove the milestone as it is set in the issue -->
/remove_milestone
