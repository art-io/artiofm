# Welcome to the ArtioFM repository

We use a set of **tools** in order to develop a set of **apps/services** that we deploy on an **infrastructure** using a set of **process** [technical procedures/human procedures] that are documented

- **process**: This session gathers all the processes put in place for the project, describes the behaviour in case of a bug, the Weekly session process and the way to create an invoice with the ERP for example.
- **tooling**: This folder lists tooling for ArtioFM, and perhaps also a description of how to set them up.
