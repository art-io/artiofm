---
title: "Assemblée constitutive"
date: 2018-11-18T12:33:46+10:00
draft: false
featured: true
weight: 2
---

# Procès-verbal d’Assemblée constitutive

Le 09.12.2022 à Fribourg.

## 1. Accueil des personnes

### a. Liste des présences

- Ludovic Delafontaine, président de l'assemblée ;
- Mathilde Ançay, facilitatrice ;
- Vincent Guidoux, rédaction du procés-verbal.

### b. Présentation et approbation de l'ordre du jour

L'ordre du jour a été accepté par l'assemblée.

## 2. Présentation du projet

Ce projet est né de l'observation d'un grand nombre de projets informatiques en Suisse Romande qui sont mis en route mais qui ne voient jamais le jour. Les membres fondateurs partagent une appréciation commune pour les choses bien faites, bien documentées et pérennes. Les membres ont aussi une envie partagée de vouloir créer une forme juridique alternative basée sur la transparence pour mettre en avant ses avantages et ses défauts et en faire profiter le plus grand nombre. Au début l'Association a pour but de s'occuper de petits mandats pour avoir une croissance organique, et ensuite s'attaquer à de plus gros projets qui ont un impact positif sur la société.

## 3. Adoption des statuts

Les statuts sont présentés à l'assemblée.

Suite à quelques discussions concernant l'orthographe et les tournures de phrases, les statuts sont acceptés par l'assemblée, à l'unanimité.

## 4. Nomination du comité

Ludovic Delafontaine, Mathilde Ançay et Vincent Guidoux sont nommés au comité d'Artios à l'unanimité.

## 5. Fixation du montant des cotisations

Un montant de CHF 100.00.- est voté à l'unanimité. Ce qui permettra de payer les premiers rendez-vous administratifs et les frais liés à la création de l'Association.

## 6. Fin de l'assemblée

Ludovic met fin à l'assemblée et remercie toutes les personnnes présentes aujourd'hui.

Ludovic Delafontaine

Mathilde Ançay

Vincent Guidoux
