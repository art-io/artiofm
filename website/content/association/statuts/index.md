---
title: "Nos statuts"
date: 2018-11-18T12:33:46+10:00
draft: false
featured: true
weight: 1
---

# Artios

## Forme juridique, but et siège

### Art. 1

Sous le nom d'Artios est créé une Association à but non lucratif régie par les présents statuts et par les articles 60 et suivants du Code civil suisse.

### Art. 2

L'Association a pour but de concevoir et réaliser des applications et fournir du conseil et du support dans le développement logiciel.

Pour atteindre ce but, l'Association met en place notamment :

- le développement d'applications demandées par des mandants ;
- du conseil, du support et de l'accompagnement dans le développement logiciel ;
- des formations dans le domaine du développement logiciel ;
- des devis pour le développement d'applications.

### Art. 3

Le siège de l'Association est à Fribourg. Sa durée est illimitée.

## Organisation

### Art. 4

Les organes de l'Association sont :

- l'Assemblée générale ;
- la ou le Secrétaire ;
- le Comité ;
- l'Organe de contrôle des comptes.

### Art. 5

Les ressources de l'Association sont constituées des dons, ou legs, par des produits des activités de l'Association et, le cas échéant, par des subventions des pouvoirs publics.

L'exercice social commence le 1er janvier et se termine le 31 décembre de chaque année.

Ses engagements sont garantis par ses biens, à l'exclusion de toute responsabilité personnelle de ses membres.

## Membres

### Art. 6

Peuvent être membres toutes les personnes ou organismes intéressés à la réalisation des objectifs fixés par l'art. 2.

Chaque membre de l'Association doit adhérer aux documents suivants :

- la charte ;
- la grille salariale et défraiement.

Ces documents annexes sont non dissociables des présents statuts et les modifications souhaitées à ceux-ci doivent être votées lors des séances de l'Assemblée générale.

Ils doivent être accessibles par les membres de l'Association et le public en tout temps.

### Art. 7

Les demandes d'admission sont adressées au Comité. Le Comité admet les nouveaux membres et en informe l'Assemblée générale.

### Art. 8

La qualité de membre se perd :

- par la démission ;
- par le non-renouvèlement explicite de vouloir rester dans l'Association à la fin de l'année civile ;
- par l'exclusion, si un membre porte préjudice à l'Association et à son image, à sa réputation et à ses intérêts.

L'exclusion est du ressort du Comité. La personne concernée peut recourir contre cette décision devant l'Assemblée générale dans un délai de 30 jours après la décision de l'exclusion.

## Assemblée générale

### Art. 9

L'Assemblée générale est le pouvoir suprême de l'Association. Elle comprend tous les membres de celle-ci.

### Art. 10

Les compétences de l'Assemblée générale sont les suivantes, elle :

- adopte l'ordre du jour de l'assemblée et approuve le procès-verbal de la dernière assemblée ;
- prend connaissance des rapports, des comptes de l'exercice et du budget et vote leur approbation ;
- donne décharge de leur mandat au Comité et à l'Organe de contrôle des comptes ;
- nomme les membres du Comité, désigne un-e sécrétaire et un Organe de contrôle des comptes ;
- adopte et modifie les statuts ;
- entend et traite les recours d'exclusion ;
- prend position sur les autres projets portés à l'ordre du jour.

L'Assemblée générale peut saisir ou être saisi de tout objet qu'il n'a pas confié à un autre organe.

### Art. 11

L'Assemblée générale se réunit au moins une fois par an sur convocation du Comité.

Le Comité peut convoquer des assemblées générales extraordinaires aussi souvent que le besoin s'en fait sentir. L'Assemblée générale extraordinaire s'organise également à la demande d'au moins un cinquième des membres de l'Association.

### Art. 12

Les assemblées sont convoquées au moins 20 jours calendaires à l'avance par le Comité. La convocation est adressée par courrier électronique et comprend l'ordre du jour de l'assemblée.

### Art. 13

Le Comité est tenu de porter à l'ordre du jour de l'Assemblée générale (ordinaire ou extraordinaire) toute proposition d'un membre présentée par courrier postal ou électronique au moins 10 jours calendaires à l'avance.

### Art. 14

L'assemblée générale est présidée par la Présidente ou le Président de l'Association ou par un autre membre proposé par le Comité.

La ou le Secrétaire de l'Association ou un autre membre du comité tient le procès-verbal de l'Assemblée ; elle ou il le signe avec la personne ayant présidée l'assemblée.

### Art. 15

Les décisions de l'Assemblée générale sont prises à la majorité simple des voix exprimées, sans tenir compte des abstentions et d'éventuels bulletins nuls. En cas d'égalité des voix, celle de la présidente ou du président de l'assemblée est prépondérante.

Les décisions relatives à la modification des statuts ne peuvent être prises qu'à la majorité des deux tiers des membres présents et représentés.

### Art. 16

Les votations ont lieu à main levée. À la demande de deux membres au moins, elles auront lieu au scrutin secret. Les membres absents ont la possibilité de donner procuration à l'un des membres de l'Association. Toutefois le membre représentant ne peut recevoir plus de deux procurations.

## Comité

### Art. 17

Le Comité exécute et applique les décisions de l'Assemblée générale. Il conduit l'Association et prend toutes les mesures utiles pour que le but fixé soit atteint. Le Comité statue sur tous les points qui ne sont pas expressément réservés à l'Assemblée générale.

### Art. 18

Le Comité se compose au minimum de deux membres et un maximum de trois membres, nommés pour un an par l'Assemblée générale et rééligibles.

### Art. 19

Le Comité se constitue lui-même. La prise de décision se fait par voie électronique pour autant qu'aucun membre du comité ne demande une délibération orale. Le cas échéant, et à défaut d'un consentement, les décisions sont prises à la majorité simple des membres présents.

### Art. 20

En cas de vacance en cours de mandat, le Comité peut se compléter par cooptation jusqu'à la prochaine assemblée générale.

### Art. 21

Les membres du Comité de l'Association travaillent de manière bénévole, sous réserve du remboursement de leurs frais effectifs.

### Art. 22

L'Association est valablement engagée par la signature d'un ou une membre du Comité.

### Art. 23

Le Comité a la charge :

- de prendre les mesures utiles pour atteindre les objectifs visés ;
- de convoquer les assemblées générales ordinaires et extraordinaires ;
- de prendre les décisions relatives à l'admission et l'éventuelle exclusion des membres ;
- de veiller à l'application des statuts, de rédiger les règlements et d'administrer les biens de l'Association ;
- de tenir les comptes de l'Association.

### Art. 24

Le Comité engage (licencie) les collaboratrices et collaborateurs salariés et bénévoles de l'Association. Il peut confier à toute personne de l'Association ou extérieure à celle-ci un mandat limité dans le temps.

## Organe de contrôle

### Art. 25

L'organe de contrôle des comptes vérifie la gestion financière de l'Association et présente un rapport à l'Assemblée générale. Il est désigné par l'Assemblée générale, en dehors des membres du comité.

## Dissolution

### Art. 26

La dissolution de l'Association est décidée par l'Assemblée générale à la majorité des deux tiers des membres présents et représentés. Elle doit être mentionnée dans la convocation à cette assemblée. L'actif éventuel sera attribué à un organisme se proposant d'atteindre des buts analogues.

Les présents statuts ont été adoptés par l'assemblée constitutive du 09.12.2022 à Fribourg.

Au nom de l'Association,

Ludovic Delafontaine

Vincent Guidoux
