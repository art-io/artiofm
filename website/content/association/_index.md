---
title: "À propos"
description: "À propos d'Artios"
---

# Une forme juridique alternative pour une entreprise - l'Association

Les membres fondateurs partagent une appréciation commune pour les choses bien faites, bien documentées et pérennes. Les membres avait aussi une envie partagée de vouloir créer une forme juridique alternative basée sur la transparence pour mettre en avant ses avantages et ses défauts et en faire profiter le plus grand nombre.

Retrouvez toutes les sources de notre travail à l'adresse suivante (en anglais) : [_Artios Association_ - gitlab.com](https://gitlab.com/artios-association)
