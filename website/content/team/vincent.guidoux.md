---
title: "Vincent Guidoux"
draft: false
image: "images/team/vincent.jpg"
jobtitle: "Membre fondateur"
promoted: true
weight: 2
---

Vincent apporte une plus-value sur les méthodologies de travail et les fonctionnalités importantes pour les clients.
