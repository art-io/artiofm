---
title: "Notre équipe"
intro_image: "images/team/ludo-et-vincent-bieres.jpg"
intro_image_absolute: false
intro_image_hide_on_mobile: false
---

# Notre équipe

Un duo complémentaire qui nous permet d'avancer de façon efficace tout en gardant un système simple, fonctionnel et évolutif.
