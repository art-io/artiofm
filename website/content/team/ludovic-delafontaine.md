---
title: "Ludovic Delafontaine"
draft: false
image: "images/team/ludovic.jpg"
jobtitle: "Membre fondateur"
promoted: true
weight: 1
---

Ludovic apporte une plus-value sur la technique et la gestion du projet au travers de ses expériences passées.
