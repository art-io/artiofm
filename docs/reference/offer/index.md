Artios   
Adresse  
NPA Ville

\begin{flushright}
Personne morale ou physique\\
Adresse\\
NPA Ville
\end{flushright}

\
\
\

Fribourg, le 16.12.2022

# Offre en régie pour TODO

| **Description**                   | **Quantité** | **Unité** |  **Prix** | **Montant** |
|-----------------------------------|-------------:|:---------:|----------:|------------:|
| Nombres d'heures de développement |           70 | heure     |       120 |        8400 |
|                                   |              |           |           |             |
|                                   |              |           | **Total** |  CHF 8400.- |

## Conditions de paiement

Cette offre est valable jusqu'au 31 décembre 2022.

Artios n'est pas assujettie à la TVA.

Le paiement doit être effectué jusqu'au 31 décembre 2022 sur le compte bancaire suivant :

Personne morale ou physique  
Adresse  
NPA Ville  
Banque  
**IBAN TODO**

## Mentions particulières

Les démarches pour l'obtention d'un compte bancaire envers la Banque Alternative Suisse au nom d'Artios est en cours. De ce fait, le paiement ne peut pas s'effectuer sur un compte appartenant officiellement à Artios et le mandataire s'engage à reverser le montant sur celui-ci une fois officialisé.

Artios

Lieu et date :

Fribourg, le 16.12.2022

Ludovic Delafontaine

\

Vincent Guidoux
