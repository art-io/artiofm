# Reference

This page contains the reference (in alphabetical order).

- [Contract example](./contract/index.md)
- [Minutes example](./minutes/index.md)
- [Offer example](./offer/index.md)
- [Specifications example](./specifications/index.md)
