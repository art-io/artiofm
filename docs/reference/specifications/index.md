# Cahier des charges

Le présent cahier des charges règle les prestations fournies par Artios pour la TODO.

# Les prestations

Voici la liste des prestations comprise dans le compte du forfait d'heures proposé à la TODO :

- Les heures de développement ;
- Les heures de mise en contact ;
- Les heures administratives ;
- La prise en main du code existant ;
- Des rendez-vous bimensuels afin de faire le point sur l'avancement du projet ;
- La création de maquettes pour avoir un support d'échanges entre toutes les parties prenantes à ce projet ;
- La maintenance ;
- La correction de bugs ;
- Effectuer les tests ;
- La rédaction d'une documentation.

# Définition des personæ et des scénarios

La définition des prochains points est approximative, le mandant ainsi que le mandataire ont connaissance qu'il n'est pas impossible que certains points soient modifiés/ajoutés/supprimés dans le futur. Ces points existent pour qu'une approximation du nombre d'heures puisse être faite.

## Personæ

TODO

## Scénarios

### Scénario 1 : TODO

En tant que TODO,  
je souhaite TODO,  
afin de TODO.
