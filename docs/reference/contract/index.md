# Contrat relatif à la fourniture de services informatiques (mandat)

Conclu entre

> **Personne morale ou physique**  
> **Adresse**  
> **NPA Ville**

ci-après le "mandant", et

> **Artios**  
> **Adresse**  
> **NPA Ville**

ci-après le "mandataire"

## 1) Objet du contrat

Le présent contrat règle les droits et obligations des parties relatifs à la fourniture de services informatiques. Le service demandeur fait appel au
mandataire pour la fourniture desdites prestations.

### Contexte

_Context_

### État actuel et développement souhaité

_Current state and expectations_

## 2) Éléments du contrat

Font partie intégrante de ce contrat, dans l'ordre de priorité suivant :

1. le présent document ;
2. le cahier des charges ;
3. l'offre du mandataire.

Par la signature du présent contrat, les parties confirment qu'elles disposent des éléments contractuels susmentionnés et qu'elles les reconnaissent dans l'ordre indiqué.

## 3) Interlocuteurs-rices, affectation de collaborateur-rice-s

Collaborateur-rice-s amené-e-s à participer à l'exécution du contrat et interlocuteur-rice-s compétent-e-s du côté d'Artios :

| **Nom et prénom du collaborateur ou de la collaboratrice**    | **Fonction**                |
|---------------------------------------------------------------|-----------------------------|
| Delafontaine Ludovic                                          | Chef de projet, Développeur |
| Guidoux Vincent                                               | Chef de projet, Développeur |

Du côté du mandataire ou de la mandataire, la responsabilité générale incombe à : 

| **Nom et prénom du collaborateur ou de la collaboratrice**    | **Fonction**                |
|---------------------------------------------------------------|-----------------------------|
| TODO                                                          | TODO                        |

## 4) Prestations du mandataire

En tant que spécialiste et conformément au but du contrat, le mandataire fournit les services suivants :

- Les heures de développement ;
- Les heures de mise en contact ;
- Les heures administratives ;
- La prise en main du code existant ;
- Des rendez-vous bimensuels afin de faire le point sur l'avancement du projet ;
- La création de maquettes pour avoir un support d'échanges entre toutes les parties prenantes à ce projet ;
- La maintenance ;
- La correction de bugs ;
- Effectuer les tests ;
- La rédaction d'une documentation.

## 5) Incombances du mandant

Le mandant est soumis aux incombances suivantes :

- Un suivi de développement bimensuel ;
- Un cahier des charges ;
- Un accès à la base de donnée sur Amazon Web Services ;
- Un accès au code sur GitHub.

## 6) Réception des prestations du mandataire

Les éléments suivants de la prestation sont réceptionnés s'ils respectent les exigences fixées au ch. 4 :

- Livraison de code sur le dépôt GitHub de TODO ;
- Livraison d'une documentation sur le dépôt GitHub de TODO.

## 7) Lieu d'exécution

Le lieu d'exécution est l'adresse du mandant, à savoir :

> **Artios**  
> **c/o Vincent Guidoux**  
> **Chemin de Bonlieu 6**  
> **1700 Fribourg**

## 8) Délais

Les délais indiqués ci-après sont contraignants, mais leur inobservation n'entraîne pas automatiquement la demeure :

- Début de la prestation : Le jeudi 22 décembre 2022 ;
- Livraison des résultats de la prestation : Le jeudi 2 mars 2023 ;
- Maintenance : Jusqu'au jeudi 11 mai 2023 ;
- Fin de la prestation : Jeudi 11 mai 2023.

## 9) Rémunération

Les prestations du mandataire sont rémunérées en régie, avec une limitation de la
rémunération (plafond des coûts) et avec une limitation des heures de travail (plafond des heures) :

Tarif horaire de 120.- CHF, avec un plafond des coûts fixé à 8400.- CHF et un plafond des heures fixé à 100 heures.

Artios n'est pas assujettie à la TVA.

## 10) Facturation, conditions de paiement, plan de paiement

Le mandataire établit une facture papier.

L'adresse de facturation est la suivante :

> **Artios**  
> **c/o Vincent Guidoux**  
> **Chemin de Bonlieu 6**  
> **1700 Fribourg**

## 11) Assurances sociales

Les prestations à fournir en vertu du présent contrat constituent une activité lucrative indépendante au regard du droit des assurances sociales. Le mandataire se charge donc de verser les cotisations pour ses collaborateurs et pour lui-même à sa caisse de compensation AVS. Le mandant ne doit au mandataire et aux collaborateurs de ce dernier ni cotisations d'assurances sociales (AVS, AI, APG, AC, etc.) ni indemnités telles qu'indemnités pour vacances, maladie, accident, invalidité ou décès.

## 12) Entrée en vigueur, modifications du contrat

Le présent contrat entre en vigueur au moment de sa signature par les deux parties.

Les modifications et compléments apportés au contrat, de même que sa résiliation, requièrent la forme écrite. Cela vaut également pour la suppression de la présente clause.

## 13) Résiliation du contrat

Le présent contrat est établi en deux exemplaires. Chaque partie reçoit un
exemplaire signé.

Les parties contractantes peuvent mettre fin en tout temps au présent contrat. Le délai de résiliation est de 30 jours. En cas de violation grave du contrat par l'une des parties, l'autre partie peut résilier ce dernier avec effet immédiat. Le droit à des dommages-intérêts est réservé. Sur demande du mandant, le mandataire cesse immédiatement de fournir ses prestations.

## 14) Expédition, signature par les parties

Le présent contrat est établi en deux exemplaires. Chaque partie reçoit un exemplaire signé. Les clauses ainsi que l'intégrité de ce contrat sont valables jusqu'au 31 décembre 2022.

**Pour le mandant :** 

TODO

Lieu et date :

\

TODO

\

**Pour le mandataire :**

Artios

Lieu et date :

Fribourg, le 16.12.2022

Ludovic Delafontaine

\

Vincent Guidoux
