# How to create an association in Switzerland

This document lists all the steps to be able, as an association, to be mandated for a job. All the necessary prerequisites to be a legal form in front of the other institutions with which we will interact.

## Introduction

In Switzerland, different legal forms exist and we have chosen the one of the association. Governed by articles 60 and following of the Swiss Civil Code as well as the [statutes of the association](../../website/content/association/statuts/index.md).

If we have chosen the association, it is for different reasons:
 - The convienance, it is enough to be two people to sign the statutes of the association and make a Constitutive Assembly.
 - It is very difficult to resell the association afterwards, if it were to be dismantled, the assets would have to be distributed to an association sharing the same goal.
 - It does not belong to anyone, if the founding members were to leave it, it can continue to exist
 - The investment, there is no need to invest 20'000.00.- CHF from the beginning to start an association, unlike the Sàrl (limited liability company).

 Please note that although we have chosen the legal form of an association, we have not chosen to be of "Public Utility". This last form of association allows to be exempted from taxes, but under certain conditions:
  - No person of the committee must be salaried, we are entitled to expenses, but no salary.
  - The confederation must recognize that we are of public utility.
  - There is a no-return clause, which makes it difficult for the association to change its purpose in the future.

For the moment, being only two, we can not afford to be of public utility, but nothing prevents that this is what will happen later.

## Creation 

To create an association, you must be at least two people, write statutes and hold a constitutive assembly. These different steps are very well defined in the following websites: [VitaminB.ch](www.vitamineb.ch) and [Bénévolat-Vaud](www.benevolat-vaud.ch). We attended a free course at Bénévolat-Vaud where we could ask all the questions we wanted.

To define the goal of our association, we were advised to see the goals of other associations on the website [Zefix](www.zefix.ch), it is also on this website that we can check if the name of the association is not already taken.

## Bank Account

One of the first steps as an association is to have a bank account. We chose the [Alternative Swiss Bank] (www.bas.ch) because it was the one that best corresponded to our values. For some banks, it is necessary to be three people in the committee of the association to have a bank account, or at least, three people who are present at the Constitutive Assembly

## Commercial Register

The registration in the commercial register is not mandatory as long as the association does not make more than 150'000.00.- CHF of revenue per year. It is a plus when talking to clients but it is not necessary.

## Insurance

Once the association is launched, a few mandates have been made and the money starts coming in, you have to think about paying your employees, insuring them, insuring the association, etc...

For this step, we were advised to contact directly an insurance agent, in order to talk about the different associations and their contracts. A contact was proposed to us with [qualibrocker](qualibroker.ch)

We have to think about the following insurances (some contact ideas):
 - AVS (Old Age and Survivors Insurance) [FER CIFA](www.cifa.ch)
 - LPP(Federal law on occupational retirement, survivors' and invalidity benefits) [nest](www.nest-info.ch)
 - Assurance perte de gain
 - Assurance RC (civil liability)

## Acknowledgements

We were able to learn about these different points thanks to the [LA FABRiꓘ](heig-vd.ch/rad/innovation-entrepreneurship/la-fabrik) which offered us an hour of consulting with **Legal Stuff Sàrl**.
