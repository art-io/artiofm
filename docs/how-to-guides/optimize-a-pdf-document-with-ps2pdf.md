# Optimize a PDF document with ps2pdf

This guide allows to optimize a PDF document that needs to be stored online to reduce its size.

## Prerequisites

- [ps2pdf](../tools/ps2pdf.md) must be installed.

## Guide

Optimize a PDF document using the following commands.

```sh
# Optimize a PDF document
ps2pdf <input>.pdf <output>.pdf
```

## Related items

These items are related to the the current item (in alphabetical order).

- _None_
