# How-to guides

This page contains all the how-to guides (in alphabetical order).

- [Convert a Markdown document to PDF with pandoc](./convert-a-markdown-document-to-pdf-with-pandoc.md)
- [Optimize a PDF document with ps2pdf](./optimize-a-pdf-document-with-ps2pdf.md)
