# Convert a Markdown document to PDF with Pandoc

This guide allows to convert a Markdown document to PDF.

## Prerequisites

- [Pandoc](../tools/pandoc.md) must be installed.

## Guide

Convert a Markdown document to PDF using the following commands.

```sh
# Convert inputs to a PDF document
pandoc \
    -V geometry:"top=2cm, bottom=2cm, left=2cm, right=2cm" \
    -V fontsize:11pt \
    -V papersize:a4 \
    -V lang:fr \
    -o <output name>.pdf <input 1>.md [<input 2>.md]
```

## Related items

These items are related to the the current item (in alphabetical order).

- [Optimize a PDF document with ps2pdf](./optimize-a-pdf-document-with-ps2pdf.md)
