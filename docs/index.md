# Documentation

Based on the [Diátaxis Documentation Framework](https://diataxis.fr) by [Daniele Procida](https://orcid.org/0000-0001-5141-7509).

- [Tutorials](./tutorials/index.md)
- [How-to guids](./how-to-guides/index.md)
- [Reference](./reference/index.md)
- [Explaination](./explaination/index.md)
