# ps2pdf

[ps2pdf](https://web.mit.edu/ghostscript/www/Ps2pdf.htm) is free and open source optimizer for PDF documents.

## How do we use this tool

We use ps2pdf to optimize our PDF documents that need to be stored online.

## Overall presentation

ps2pdf is a command line tool that wraps Ghostscript to convert PDF documents to PostScript documents and back.

## Why did we choose this tool

- Fast;
- Easy to use.

## Related items

These items are related to the the current item (in alphabetical order).

- [Optimize a PDF document with ps2pdf](../how-to-guides/optimize-a-pdf-document-with-ps2pdf.md)

## Additional resources

These items are additional resources to the the current item (in alphabetical order).

- _None_
