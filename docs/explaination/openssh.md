# OpenSSH

[OpenSSH](https://www.openssh.com/) is a set of tools to access machines using the SSH protocol, generate and manage SSH keys.

## How do we use this tool

We use OpenSSH to access our virtual machines using pairs of SSH keys to avoid the need to access them using an insecure password.

## Overall presentation

SSH is a protocol that uses two cryptographic keys to access remote systems, a public key and a private key.

- The public key can be shared with any one and is sent to the remote system.
- The private key must not be shared and must be kept securely.

When a client connects to the remote system, both keys will be used to authenticate the client and allow the access to the remote system.

## Technical details

These are technical details that can be done or needed to be done first when using this tool.

### Create a pair of SSH keys

```sh
# Generate the pair of SSH keys of type ED 25519
ssh-keygen \
    -t ed25519 \
    -f ~/.ssh/my-new-ssh-key \
    -q \
    -N "" \
    -C ""
```

### Copy the public key to the virtual machine

```sh
# Copy the public key to the virtual machine
ssh-copy-id -i ~/.ssh/my-new-ssh-key.pub <virtual machine user>@<virtual machine hostname or ip>
```

### Access the virtual machine

```sh
# Access the virtual machine with a specific ssh key
ssh -i ~/.ssh/my-new-ssh-key <virtual machine user>@<virtual machine hostname or ip>
```

## Why did we choose this tool

- The standard;
- More secure than a standard password.

## Related items

These items are related to the the current item (in alphabetical order).

- [Ungleich](./ungleich.md)
- [WireGuard](./wireguard.md)

## Additional resources

These items are additional resources to the the current item (in alphabetical order).

- _None_
