# Ungleich

[Ungleich](https://ungleich.ch/) is a Swiss cloud provider.

## What do we use from this provider and requirements

We use their [IPv6 Only Hosting](https://ipv6onlyhosting.com/) product to host our virtual machines (VMs).

A credit card is needed to use this provider.

## Overall presentation

The virtual machines run on Debian.

The [dashboard - ipv6onlyhosting.com](https://ipv6onlyhosting.com/en-us/hosting/dashboard) offers a simple UI to manage the VMs and the SSH keys used to log on the virtual machines.

The virtual machine is manageable through the [desktop web application - desktop.ungleich.ch](https://desktop.ungleich.ch) or though SSH via IPv6 (the IPv6 WireGuard VPN might be required).

Ungleich has a [wiki - redmine.ungleich.ch](https://redmine.ungleich.ch/projects/open-infrastructure/wiki) regarding their infrastructure. Here are a few pages that are worth having a look at if management is necessary:

- [How to use the IPv4-to-IPv6-Proxy - redmine.ungleich.ch](https://redmine.ungleich.ch/projects/open-infrastructure/wiki/How_to_use_the_IPv4-to-IPv6-Proxy)
- [Ungleich IPv6 wireguard VPN - redmine.ungleich.ch](https://redmine.ungleich.ch/projects/open-infrastructure/wiki/Ungleich_IPv6_wireguard_VPN)
- [I want to give my VM to somebody else to take over, is it possible? - redmine.ungleich.ch](https://redmine.ungleich.ch/projects/open-infrastructure/wiki/FAQ_at_Data_Center_Light#Q-I-want-to-give-my-VM-to-somebody-else-to-take-over-is-it-possible)

The support can be contacted via email at the email address [`support@ungleich.ch`](mailto:support@ungleich.ch) or via the [contact - ungleich.ch](https://ungleich.ch/u/contact/) page.

## Technical details

These are technical details that need to be considered or needed to be done first when using this provider.

### Add a collaborator to Ungleich

At the moment, it is not possible to manage an Ungleich product by many different people. Read more here: [I want to give my VM to somebody else to take over, is it possible? - redmine.ungleich.ch](https://redmine.ungleich.ch/projects/open-infrastructure/wiki/FAQ_at_Data_Center_Light#Q-I-want-to-give-my-VM-to-somebody-else-to-take-over-is-it-possible)

### Add a SSH key

SSH keys can be added to Ungleich through the [My SSH keys - ipv6onlyhosting.com](https://ipv6onlyhosting.com/en-us/hosting/ssh_keys) interface.

### Configure / setup our website to be reachable by IPv4

As stated in the [Ungleich wiki - How to use the IPv4-to-IPv6-Proxy - redmine.ungleich.ch](https://redmine.ungleich.ch/projects/open-infrastructure/wiki/How_to_use_the_IPv4-to-IPv6-Proxy), a few steps are necessary to access our website on our IPv6 virtual machines.

1. Add an `AAAA` DNS entry pointing to the virtual machine
2. Inform the Ungleich team about your domain

    > **To**: support@ungleich.ch\
    > **Subject**: Add our domain to your IPv4-to-IPv6 proxy\
    > **Content**:
    > 
    > Hello,
    >
    > I would like to add the following domain name to the whitelist of the Ungleich IPv4-to-IPv6 proxy:
    >
    > \- Virtual machine: `IP of the virtual machine`\
    > \- Domain name: `artios.ch`
    >
    > I already have done all the steps described in your wiki (https://redmine.ungleich.ch/projects/open-infrastructure/wiki/How_to_use_the_IPv4-to-IPv6-Proxy) so everything should be fine.
    >
    > Thank you in advance,\
    > Ludovic Delafontaine

3. Ungleich team will setup the proxy to whitelist our domain
4. Add a `A` DNS entry pointing to Ungleich IPv4-to-IPv6 proxy

    - `185.203.112.54` (if the virtual machine has the IPv6 address `in the range 2a0a:e5c0::/48`)
    - `185.203.115.0` (if the virtual machine has the IPvIPv6 address in the range `2a0a:e5c0:2::/48`)

When everything is set up, the domain `artios.ch` should be redirected to our virtual machine, even from an IPv4 address.

### Access the virtual machine from the IPv4 world with WireGuard

As stated in the [Ungleich wiki - Ungleich IPv6 wireguard VPN - redmine.ungleich.ch](https://redmine.ungleich.ch/projects/open-infrastructure/wiki/Ungleich_IPv6_wireguard_VPN), a few steps are necessary to access our website on our IPv6 virtual machines.

1. Install WireGuard
2. Create a private key

    ```sh
    umask 077; wg genkey > privkey
    ```

3. Get the public key from the private key

    ```sh
    wg pubkey < privkey
    ```

4. Send the public key to the Ungleich team

    > **To**: support@ungleich.ch\
    > **Subject**: Add my public key to your IPv6 WireGuard VPN\
    > **Content**:
    > 
    > Hello,
    >
    > I would like to add the following public key to the Ungleich IPv6 WireGuard VPN:
    >
    > \- WireGuard public key: `WireGuard public key`
    >
    > Thank you in advance,\
    > Ludovic Delafontaine

5. Ungleich team will setup the WireGuard VPN to access the virtual machine

When everything is set up, you should be able to access the Ungleich IPv6 network (even from an IPv4 address) with WireGuard and then access the virtual machine.

## Why did we choose this provider

- Hosted in Switzerland;
- Ecofriendly;
- Willingness to be future proof (with IPv6);
- Cheap.

## Related items

These items are related to the the current item (in alphabetical order).

- [Infomaniak](./infomaniak.md)
- [WireGuard](./wireguard.md)

## Additional resources

These items are additional resources to the the current item (in alphabetical order).

- [whynoipv6.com - Why No IPv6? - whynoipv6.com](https://whynoipv6.com/)
