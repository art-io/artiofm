# WireGuard

[WireGuard](https://www.wireguard.com/) is a simple yet powerful VPN.

## How do we use this tool

We use WireGuard to access Ungleich's network.

## Overall presentation

WireGuard uses a simple configuration file to access other peers.

```ini
[Interface]
PrivateKey = <Private key of the client>
ListenPort = <The listening port>
Address = <IP range for the client>
DNS = 2a0a:e5c0:2:12:0:f0ff:fea9:c451, 2a0a:e5c0:2:12:0:f0ff:fea9:c45d

[Peer]
PublicKey = <Ungleich WireGuard VPN server public key>
AllowedIPs = ::/0
Endpoint = <The endpoint of the Ungleich WireGuard VPN server>
```

## Technical details

These are technical details that can be done or needed to be done first when using this tool.

### Create a private key

```sh
# Create a WireGuard private key
umask 077; wg genkey > privkey
```

### Get the public key from the private key

```sh
# Get the WireGuard public key from the private key
wg pubkey < privkey
```

## Why did we choose this tool

- Fast;
- Modern;
- Simple to install and configure;
- Compatible with many systems with many clients (Windows, Linux, macOS, Android, ...)

## Related items

These items are related to the the current item (in alphabetical order).

- [OpenSSH](./openssh.md)
- [Ungleich](./ungleich.md)

## Additional resources

These items are additional resources to the the current item (in alphabetical order).

- [Configure a Wireguard interface (wg) - alpinelinux.org](https://wiki.alpinelinux.org/wiki/Configure_a_Wireguard_interface_(wg))
