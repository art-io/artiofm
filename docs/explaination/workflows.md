# Workflows

This file describes different workflows that the team follows.

## Milestone calendar

| Monday | Tuesday | Wednesday | Thursday | Friday |
|---|---|---|---|---|
|  |  |  | Day 1<br>Milestone session | Day 2<br>Daily scrum |
| Day 3<br>Daily scrum | Day 4<br>Daily scrum | Day 5<br>Daily scrum | Day 6<br>Weekly session | Day 7<br>Daily scrum |
| Day 8<br>Daily scrum | Day 9<br>Daily scrum | Day 10<br>Daily scrum |  |  |

## Daily routine

During the day, we may have an idea for the next step in the project, a question for the team, a direction for the code. In this case, you should create an Issue with the label ~Sorting::In-Basket, with a title that lets people know what you mean. That way, you don't have to think about it anymore and the team will talk about it in the Weekly session.

```plantuml
@startuml
partition "daily routine" {
  :Got any idea ?;
  :Create an Issue with the label Sorting::In-Basket;
}
@enduml
```

The work cycle of each person in the team is like this:
* There is always a part where something is produced. It is always represented by an Issue.
* After producing something, we will review the production of the other people in the team.
* Then we will participate in the planning of the next milestone by creating an Issue that completes the Epics already created.


```plantuml
@startuml
partition "daily routine" {
  :Is there one Issue not started in the milestone ?;
  ->yes;
  repeat : Choose an Issue
    partition "producting" {
      :Create a branch;
      :Create a merge request;
      :Work on it;
      :Setup the merge request to __ready__;
    }
    partition "review" {
      :Do a review of an open merge request;
    }
    partition "refinement" {
      :Take the next Issue, which does not exist, needed to advance in the Epics;
      if (Are we waiting on someone ?) then (yes)
        :Create an Issue with the label Sorting::Waiting;
      endif 
      : Create an Issue and add it to the next milestone;
    }
  repeat while (Is there one Issue not started in the milestone ?) is (yes)
}
@enduml
```


## Daily scrum

**Objectives**
* Expressing feelings out of 5
* To report on work done
* Expressing confidence/feeling about the task
* Potentially ask for help
* Reassess the purpose of the task in hand
* Saying what is going to be done during the day

**Course of action**

Duration: 10 minutes

* The milestone leader manages the session
* He gives the floor to the participants one after the other
* Each one briefly presents the work done the previous day
  * May show results

## Weekly session

Every week, the team will go through all the Issues with the label ~Sorting::In-Basket to sort them out.

This is when the discussions take place. They are usually quite quick. If everyone has the same opinion, a decision is made without listening to each person's opinion.

```plantuml
@startuml
partition "weekly session" {
  :Is there an Issue with the label Sorting::In-Basket ?;
  ->yes;
  repeat : Are we going to do it ?
    switch ()
      case ( Discussion )
        :Change the label to Sorting::Discussion, and resolve it;
      case ( Absolutely not )
        :Change the label to Sorting::Trash, without comment;
      case ( No ) 
        :Change the label to Sorting::Reference, with comment;
      case ( Maybe )
        :Change the label to Sorting::Maybe, with comment;
      case ( yes )
        :Create an Epic;
    endswitch
  repeat while (Is there an Issue with the label Sorting::In-Basket) is (yes)
}
@enduml
```

## Milestone session

The Milestone meeting is like the Weekly meeting, except that the team confirms whether the next milestone is well defined or not.

## Weights

For each task with the label ~Sorting::Sorted, a certain weight must be given in order to evaluate its feasibility in an Epic.

It is a numerical value assigned to a task. This value represents an estimate based on a reflection on the 4 criteria below:

* Effort / Volume: How much work do I have to do?
* Risk: How much is the integrity of the application at risk?
* Complexity: How complex are the designs and algorithms I will be dealing with?
* Uncertainty: How much technical knowledge are we missing to complete the task?

For each criterion, we will estimate from 0 to 5 (including bounds). Then add up each value of the criteria. And finally, this result will undergo a higher margin to be a number of Fibonnaci(0,1,2,3,5,8,13,21)

Exemple:

| Issue | Volume | Risk | Complexity | Uncertainty | Total | Weight |
|---|---|---|---|---|---|---|
| #1 | 1 | 0 | 0 | 0 | 1 | 1 |
| #2 | 4 | 1 | 2 | 5 | 12 | 13 |
| #3 | 1 | 0 | 2 | 1 | 4 | 5 |
| #4 | 2 | 3 | 1 | 0 | 6 | 8 |
| #5 | 3 | 3 | 3 | 5 | 14 | 21 |
| #6 | 1 | 0 | 1 | 1 | 3 | 3 |
