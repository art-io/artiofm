# Tooling

TODO

This page table tooling for ArtiosFM
| Name | Version | Comment |
|---|---|---|
| [Integram](https://integram.org/) | 2 | Framework and platform to integrate services with Telegram using the official Telegram Bot API |


This table lists GitLab features used for ArtiosFM
| Name | GitLab Version | Comment |
|---|---|---|
| [Webhooks](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) | 15.4 | Used with Integram |
| [Description templates](https://docs.gitlab.com/ee/user/project/description_templates.html#example-description-template) | 15.4 | Used to define Issue templates |
