# Pandoc

[Pandoc](https://pandoc.org/) is free and open source universal document converter.

## How do we use this tool

We use Pandoc to convert our Markdown documents to PDF documents.

## Overall presentation

Pandoc is a command line tool that converts one markup format to another. Have a look at the [Demos - pandoc.org](https://pandoc.org/demos.html) for examples.

## Why did we choose this tool

- Fast;
- Easy to use.

## Related items

These items are related to the the current item (in alphabetical order).

- [Convert a Markdown document to PDF](../how-to-guides/convert-a-markdown-document-to-pdf-with-pandoc.md)

## Additional resources

These items are additional resources to the the current item (in alphabetical order).

- _None_
