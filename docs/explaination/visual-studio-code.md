# Visual Studio Code

[Visual Studio Code](https://code.visualstudio.com/) is free and open source IDE.

## How do we use this tool

We use Visual Studio Code to develop our project.

## Overall presentation

[Extensions - marketplace.visualstudio.com](https://marketplace.visualstudio.com/VSCode) allow to add features to Visual Studio Code.

[Settings - code.visualstudio.com](https://code.visualstudio.com/docs/getstarted/settings) are set in a simple JSON file that can be shared among the team.

## Why did we choose this tool

- Lightweight;
- Easy to use;
- Many extensions available.

## Related items

These items are related to the the current item (in alphabetical order).

- _None_

## Additional resources

These items are additional resources to the the current item (in alphabetical order).

- _None_
