# Infomaniak

[Infomaniak](https://www.infomaniak.com/) is a Swiss cloud provider.

## What do we use from this provider and requirements

We use their [Domain Name](https://www.infomaniak.com/en/domains/) and their [Mail Service](https://www.infomaniak.com/en/hosting/service-mail/) products to have a DNS record and a single email address.

A credit card is needed to use this provider.

## Overall presentation

The [manager](https://manager.infomaniak.com/) is the main place to buy and configure Infomaniak products.

Each Infomaniak product has its own place. Here are a few links to access these services without passing by the Infomaniak manager:

- [Emails](https://mail.infomaniak.com)

## Technical details

These are technical details that need to be considered or needed to be done first when using this provider.

### Add a user to Infomaniak

On the left side bar of the Infomaniak manager interface, select **User management** > **Users**. Select **Add a user**. Select the user's role and permissions (**Staff member**, enable **Authorise receipt (email + SMS) of billing reminder** and **Allow (email) reception of product events**). Select **Do not integrate in a work team**. Select **Do not create a new email address**. Enter the email addrees of the new user and select **Continue**.

### Create a work team

On the left side bar of the Infomaniak manager interface, select **User management** > **Work teams**. Select **Add a work team**. Choose a name and a color for the new work team (_Artios_ and an orange color). Select the product(s) the team members will have access to.

- Domain `artios.ch`
- Email service `artios.ch`

Select **Continue**. Select **Finish**.

### Add a user to work team

On the left side bar of the Infomaniak manager interface, select **User management** > **Work teams**. Select the work team (**_Artios_**). To add a new user to the work team, select the **+** sign.

### Add a DNS record

In the main Infomaniak manager interface, under **Products**, select **Domains**. On the left side bar of the Infomaniak manager interface, select **DNS zone**. Select **Add an entry** and fill the form for the required entry.

As a remainder, here are some commonly used DNS records:

- `A` record: Redirect a domain to an IPv4 address.
- `AAAA` record: Redirect a domain to an IPv6 address.
- `CNAME` record: Redirect a domain to another domain name.
- `TXT` record: A text field that is used by some services to validate the ownership of the domain.

### Create a new email address

In the main Infomaniak manager interface, under **Products**, select **Email service**. Select **Create an email address**. Give a name to the new email address (**_contact@artios.ch_**). Select **Continue**. Select **For one or more users**. Select **Continue without inviting**. Set a password to access the email. Select **Continue**. Select **Finish**.

### Add an existing email address to [mail.infomaniak.com](https://mail.infomaniak.com)

In the left side of the [mail.infomaniak.com](https://mail.infomaniak.com) interface, select **Add an existing address**. Set the email address and password and select **Add**.

### Add an email alias

In the main Infomaniak manager interface, under **Products**, select **Email service**. Select the email address (**_contact@artios.ch_**). Select **Redirection and alias**. Select **Add alias**. Set the alias email address and select the **+** sign. Select **Approve**.

### Set filters and rules

In the main Infomaniak manager interface, under **Products**, select **Email service**. Select the email address (**_contact@artios.ch_**). Select **Filters and rules**. Select **Create a rule** and fill the form for the required rule.

The following rules have been made:

- Name: **to:\<alias\>@artios.ch**
- If an incoming message matches: **All conditions**
- Condition: **To** **Is equal to** **`<alias>@artios.ch`**
- Execute following actions: **Move to the folder** **Alias**

### Set automatic reply

In the main Infomaniak manager interface, under **Products**, select **Email service**. Select the email address (**_contact@artios.ch_**). Select **Automatic reply and signatures**. Select **Add** under _Automatic reply_ and fill the form for the required automatic reply.

When done, do not forget to enable the automatic reply.

### Set signatures

In the main Infomaniak manager interface, under **Products**, select **Email service**. Select the email address (**_contact@artios.ch_**). Select **Automatic reply and signatures**. Select **Add** under _Automatic reply_ and fill the form for the required automatic reply.

The following signatures have been made:

- Name: **:\<Alias\> - \<language\>**
- Full name: **\<First name\> \<Last name\>**
- Signature positioning: **After the preceding messages**

## Why did we choose this provider

- Hosted in Switzerland;
- Well known for its quality and customers service;
- Ecofriendly.

## Related items

These items are related to the the current item (in alphabetical order).

- [Ungleich](./ungleich.md)

## Additional resources

These items are additional resources to the the current item (in alphabetical order).

- _None_
