# Glossary

Here is a small glossary of words that the team uses on a daily basis. We have put the correspondence with other instances or with our inspirations.

## Agile

| Agile artifact | GitLab feature | Description |
|---|---|---|
| User story | Issues | In Agile, you often start with a user story that captures a single feature that delivers business value for users |
| Epic | Epics | Some Agile practitioners specify an abstraction above user stories, often called an Epic, that indicates a larger user flow consisting of multiple features |
| Points and estimation | Weights | It's the level of technical effort that is estimated for each in-scope user story |
| Sprint/iteration | Milestones | A sprint represents a finite time period in which the work is to be completed, which may be a week, a few weeks, or perhaps a month or more |
| Agile board | Issue boards | Throughout the sprint, Issues move through various stages("TO DO", "DONE", etc..), depending on the workflow in your particular organization |

Reference: [How to use GitLab for Agile Software development](https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/)

## Refinement

| Getting Things Done (Vocabulary) | Our implementation | Description |
|---|---|---|
| In-basket | Issue with ~Sorting::In-Basket | This is where we will put all our ideas during the week. Once a week, we will go through this list and sort through the other lists |
| Waiting | Issue with ~Sorting::Waiting | Actions for which we expect someone to return |
| Maybe | Issue with ~Sorting::Maybe | These are the projects that we might do one day |
| Reference | Issue with ~Sorting::Reference | These are the projects that we are not going to do, but that we keep under our belts |
| Trash | Issue with ~Sorting::Trash | These are projects that we would do for nothing in the world |
| Action | Issue with ~Sorting::Sorted | These are the tasks to be done |
| Project | Epic with ~Sorting::Sorted | It is the purpose of what we want to do, the actions allow us to reach this goal |

Reference: [[Hors sujet] La méthode Getting Things Done (GTD)](https://scienceetonnante.com/2022/07/25/getting-things-done-gtd/)
