# Learning how to set triage up

*Highly inspired by:*
 - [105 - Issue Automation - Keeping your issues squeaky clean](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/getting-started/105/)
 - [GitLab Triage Project - README.md](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage)
 - [Gitlab - Trigger pipelines by using the API](https://docs.gitlab.com/ee/ci/triggers/index.html)
 - [Medium - GitLab Triage Bot](https://medium.com/analytics-vidhya/gitlab-triage-bot-ba8afca4440a)

## Introduction

When you use issues and labels to manage day to day work and define a specific process, often there are situations and times where it's hard to keep labels in the right status. 

We need to define a set of rules that will automatically

 - Add a label when it is missing
 - Change or remove a label when it's wrong
 - Add a comment, mention a user, and more thorough comments and `/quick` [actions](https://docs.gitlab.com/ee/user/project/quick_actions.html).

The [Triage Bot](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage) is an Open GitLab project that makes it possible to automate many issue and merge request hygiene tasks to ensure that your projects are more consistently following a given process.

## Overview

In this tutorial, you will set the Triage Bot up. At first, we will manual activate the Triage Bot to add a comment on every opened issue on your project, then every time a issue is created and lack a label, it will be added automatically.

## Checking the prerequisites / requirements

This tutorial assume you know your ways around GitLab. It also assumes you know the basics of Git.

## Disclaimer

As this tutorial assumes you know how to use Git, when we will ask you to add a file to your repository, we will assume that you add it to your online repository by commiting it and pushing it on the main branch.

## Step 0: Creating the tutorial environment

### Creating the project on GitLab

First create a blank project on GitLab:

 - **Project name**: Learning how to set Triage Bot up
 - **Project URL**: https://gitlab.com/[YOUR-NAMESPACE]/learning-how-to-set-triage-up
 - **Project deployment target (optional)**: No deployment target
 - **Visibility Level**: Private
 - Enable **Initialize repository with a README** 
 - Don't enable **Enable Static Application Security Testing (SAST)**

### Creating an issue

We need an issue to work with:

  - In your project select the left menu option **Issues > List**
  - Click **New issue**
  - Create an issue with the **Title** : "The triage bot is set up"

### Creating a label

We need a label "New" to labelise new issues:
  - In your project select the left menu option **Project Information > Labels**
  - Click **New label**
  - Create a label:
    - **Title**: New
    - **Description**: Used to labelise new issues
    - **Background color**: #bada55

## Step 1: Setting the configuration and GitLab CI files up

For detailed instructions about rules and policy options read the [Defining a Policy](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage#defining-a-policy) section in the documentation of the Triage Bot project.

At the root level of your Git repository, create your first triage policy `.triage-policies.yml`:

```yaml
resource_rules:
  issues:
    rules:
      - name: find all open issues - any label - and add a comment
        conditions:
          state: opened
        actions:
          comment: |
            {{author}} has created an issue!
```

The Triage Bot runs as a scheduled CI pipeline job, so your project will need to define a `.gitlab-ci.yml` file. The `.gitlab-ci.yml` file is stored in the root directory of your project.

```yaml
stages:
  - triage

policy:run:
  image: ruby:3.2-rc-alpine
  stage: triage
  script:
    - gem install gitlab-triage
    - gitlab-triage --token $API_TOKEN --source projects --source-id $CI_PROJECT_PATH
  rules:
    - when: manual
```

## Step 2: Configuring CI/CD Environnement Variable

The Triage Bot needs to have access and permission to your GitLab project to read and update issues. This is called a "token", and is stored in GitLab using a "CI/CD Variable" - in this case it is the `$API_TOKEN` that you see in the `.gitlab-ci.yml` file.

### Getting your API Token

 - The API Token is linked to your account and basically gives the Bot permission to act on your behalf. Select **Preferences** in the dropdown menu of your profile that is in the top-right corner of GitLab.
 - Select `Access Tokens`
 - Create a **Personal Acces Token**
   - **Token name**: Triage Bot Token
   - **Expiration date**: [blank] (unless you want it to expire)
   - **Select scopes**: Select "api"
 - Click **Create personal access token**
 - **Your new personal access token** will appear at the top of the page.
 - Copy the API Token for later

### Setting Up CI-CD API Token Variable

  - In your project select the left menu option **Settings > CI/CD**
  - Then expand the `Variables` section
  - Click **Add Variable**:
    - **Type**: Variable
    - **Key**: API_TOKEN
    - **Value**: Your API Token - The one you copied before
    - **Environment scope**: *let the default option*
    - Enable **Protect variable**
    - Enable **Mask Variable**
    - Enable **Expand variable reference**
  - Click **Add Variable**

## Step 3: Running your first manual Triage Policy 

In your project select the left menu option **CI/CD > Jobs**.

Run the `policy:run` job with the play button on the right.

You have run your first Triage Policy! When the `policy:run` job is finished, you can go see that the issue you created earlier is commented.

## Step 4: Running your Triage Policy everytime there is a change on the ticket

Imagine you have a workflow with your colleague, that every time you create an issue, it should be labelled "New". From time to time, people can forget to do so, so we will automatize it:

### Setting up the files

First, modify your `.triage-policies.yml` file :

```yaml
resource_rules:
  issues:
    rules:
      - name: Set "New" label to no labelled issues
        conditions:
          state: opened
          labels:
            - None
        actions:
          labels:
            - New
```

And modify your `.gitlab-ci.yml` file :

```yaml
stages:
  - triage

trigger:policy:run:
  image: ruby:3.2-rc-alpine
  stage: triage
  script:
    - gem install gitlab-triage
    - gitlab-triage --token $API_TOKEN --source projects --source-id $CI_PROJECT_PATH
  rules:
    - if: $CI_PIPELINE_SOURCE == 'trigger'
```

### Setting Up Pipeline triggers

  - In your project select the left menu option **Settings > CI/CD**
  - Then expand the **Pipeline triggers** section
  - Add a new line where
    - **Description**: "Pipeline trigger
  - Note the Token somewhere
  - Under the Token you'll see some example how to trigger this project's pipeline, note the URL for the **Webhook**:
     - _Looks like that: `https://gitlab.com/api/v4/projects/PROJECT_ID/ref/REF_NAME/trigger/pipeline?token=TOKEN&variables[RUN_NIGHTLY_BUILD]=true` (replace `REF_NAME` with `main` and `TOKEN` with the Token you just created)_"

### Setting Up Webhooks

  - In your project select the left menu option **Settings > CI/CD**
  - Then expand the **Webhooks** section
  - Create a new Webhooks
    - **URL**: the one you juste noted
    - **Trigger**: Issues events

Now, create an issue with the title "Triage bot add label to unlabeled issue" and wait for 20-40 secondes and a label "New" will be added to the issue. Hooray! You have automated the triage of an issue.
