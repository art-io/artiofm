# `.vscode`

This directory is used to share common configuration files for [Visual Studio Code](../docs/tools/visual-studio-code.md).

- [`settings.json`](./settings.json) contains the common settings for Visual Studio Code.
